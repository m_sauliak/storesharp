﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Store3.Models
{
    public class User
    {
        // ID 
        [Key]
        public int Id { get; set; }

        //Name of user
        [Required(ErrorMessage = "Field can't be empty")]
        [Display(Name = "Name")]
        [MaxLength(50, ErrorMessage = "The text entered exceeds the maximum length")]
        public string Name { get; set; }

        //Middle name of user
        [Required(ErrorMessage = "Field can't be empty")]
        [Display(Name = "Middle Name")]
        [MaxLength(50, ErrorMessage = "The text entered exceeds the maximum length")]
        public string MiddleName { get; set; }

        //Last name of user
        [Required(ErrorMessage = "Field can't be empty")]
        [Display(Name = "Last Name")]
        [MaxLength(50, ErrorMessage = "The text entered exceeds the maximum length")]
        public string LastName { get; set; }

        //Email address
        [Display(Name = "Email address")]
        [EmailAddress]
        [Required(ErrorMessage = "The email address is required")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        //Confirmation Email address
        [Display(Name = "Email confirmation address")]
        [EmailAddress]
        [Required(ErrorMessage = "The confirmation email address is required")]
        [Compare("Email", ErrorMessage = "The email and confirmation email do not match.")]
        public string ConfirmationEmail { get; set; }

        //User's password
        [Required]
        [Display(Name = "Password")]
        [UIHint("Password")]
        [DataType(DataType.Password)]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длина записи")]
        public string Password { get; set; }


        //Confirmation password
        [Required]
        [Display(Name = "Confirmation Password")]
        [UIHint("Confirmation Password")]
        [MaxLength(50, ErrorMessage = "The text entered exceeds the maximum length")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmationPassword { get; set; }


        //Age
        [Required(ErrorMessage = "Field can't be empty")]
        [Display(Name = "Age")]
        public int Age { get; set; }

        //Phone number
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string PhoneNumber { get; set; }

        //Other details text block        
        [Display(Name = "Details")]
        [MaxLength(100, ErrorMessage = "The text entered exceeds the maximum length")]
        public string Details { get; set; }

        [Required]
        [Display(Name = "Role")]
        public int RoleId { get; set; }
        public Role Role { get; set; }

        //Country 
        [Range(1, int.MaxValue, ErrorMessage = "Select a country")]
        public Countries Country { get; set; }

        //City
        [Range(1, int.MaxValue, ErrorMessage = "Select a city")]
        public Cities City { get; set; }

        public enum Countries
        {
            Belarus = 1,
            Belgium,
            Netherlands,
            France,
            Russia,
            United_Kingdom,
            USA
        }

        public enum Cities
        {
            Mogilev = 1,
            Gomel,
            Vitebsk,
            Minsk,
            Moscow,
            London,
            Washington
        }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}