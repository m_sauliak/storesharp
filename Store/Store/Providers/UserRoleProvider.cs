﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Store.Models;
using System.Web.Helpers;
using System.Web.WebPages;
using Microsoft.Internal.Web.Utils;

namespace Store.Providers
{
    public class UserRoleProvider : RoleProvider
    {
        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string login)
        {
            string[] role = new string[] { };
            using (Models.AppContext db = new Models.AppContext())
            {
                try
                {
                    // user from db
                    User user = (from u in db.Users
                                 where u.Email == login
                                 select u).FirstOrDefault();
                    if (user != null)
                    {
                        // role
                        Role userRole = db.Roles.Find(user.RoleId);

                        if (userRole != null)
                        {
                            role = new string[] { userRole.Name };
                        }
                    }
                }
                catch
                {
                    role = new string[] { };
                }
            }
            return role;
        }    

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            bool compareResult = false;
            
            using (Models.AppContext _db = new Models.AppContext())
            {
                try
                {
                    // current user
                    User user = (from u in _db.Users
                                 where u.Email == username
                                 select u).FirstOrDefault();
                    if (user != null)
                    {
                        // role
                        Role userRole = _db.Roles.Find(user.RoleId);

                        // compare result
                        if (userRole != null && userRole.Name == roleName)
                        {
                            compareResult = true;
                        }
                    }
                }
                catch
                {
                    compareResult = false;
                }
            }
            return compareResult;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}
